

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="entities.Pessoa"%>
<%@page import="java.util.List"%>
<%@page import="entities.Tarefa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Tarefa tarefa = (Tarefa) request.getAttribute("tarefa");%>

<html>
<head>
 <meta content="text/html; charset=ISO-8859-1" http-equiv="content-type"> 
  <title>Editar Tarefa</title>
</head>
<body>
<h1>Editar Tarefa</h1>
<br><form name="tarefa" action="salvarTarefa" method="get">
<table style="text-align: left; width: 100%;" border="1" cellpadding="2" cellspacing="2">
  
  <tbody>
    <tr>
        <td>Nome:</td>
        <td> <input type="text" name="nome" value="<%= tarefa.getNome() %>" rows="1" cols="40" ></input></td>
    </tr>
    <tr>
      <td>Data de inicio:</td>
      <td><input type="text" name="dataDeInicio" rows="1" value="${data}" ></input></td>
    </tr>
    <tr>
        <td>Percentual Concluido:</td>
        <td><input type="text" name="percentualConcluido" rows="1" value="<%= tarefa.getPorcentual_concluido() %>" ></input>
        <input type="hidden" name="id" value="<%= tarefa.getId_tarefa() %>"></input></td>
        
    </tr>
  </tbody>
  
</table>

<br>

        <input type="submit" value="Salvar modificações"></input></br>
        </form>
<h2>Pessoas respons&aacute;veis</h2>
<form action="adicionarResponsavel" method="get" class="form round">
    ID do novo responsavel:<input type="text" name="idPessoa" >
    <input type="hidden" name="id_tarefa" value="${tarefa.id_tarefa}">
    <input type="submit" value="Adicionar Responsavel">
</form>
<br/>
    
    <%-- --%>   
        <c:choose>
            <c:when test="${pessoasNR.size() == 0}">
                NÃ£o hÃ¡ outras pessoas que nao sejam responsaveis
            </c:when>
            <c:otherwise>
                <form action="adicionarResponsavelCombobox">
                <input type="hidden" name="id_tarefa" value="${tarefa.id_tarefa}"></input>
                <select name="idResponsavel">     
                    <c:forEach var="p" items="${pessoasNR}">
                        <option value="${p.idPessoa}"> ${p.nome} (id=${p.idPessoa}) </option> 
                    </c:forEach>
                </select>
                <input type="submit" value="Adicionar Essa Pessoa">
                </form>
            </c:otherwise>
        </c:choose>
 

  <%-- --%>
    
    
    
<table style="text-align: left; width: 100%;" border="1" cellpadding="2" cellspacing="2">    
  <tbody>
     <c:forEach var="r" items="${tarefa.responsaveis}">
        <tr><td>${r.nome}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><a href="excluirResponsavel?id_tarefa=${tarefa.id_tarefa}&idPessoa=${r.idPessoa}">Excluir</a></td></tr>       
     </c:forEach>
     <br/>
  </tbody>
</table>

<br>

<h2>Tarefas Dependentes</h2>
<c:choose>
    <c:when test="${tarefas.size() == 0}">
        Não há mais dependencias possiveis
    </c:when>
    <c:otherwise>
        <form action="adicionarDependente" >
        <input type="hidden" name="id_tarefa" value="${tarefa.id_tarefa}"></input>
        <select name="idDependencia">     
            <c:forEach var="t" items="${tarefas}">
                <option value="${t.id_tarefa}">${t.nome}</option> 
            </c:forEach>
        </select>
        <input type="submit" value="Adicionar Nova Tarefa Dependente">
        </form>
    </c:otherwise>
</c:choose>
<table style="text-align: left; width: 100%;" border="1" cellpadding="2" cellspacing="2">
  <tbody>
      <c:forEach var="t" items="${tarefa.dependencias}">
        <tr>
            <td>${t.nome}</td>
            <td><a href="excluirDependente?id_tarefa=${tarefa.id_tarefa}&idDependencia=${t.id_tarefa}">Excluir</a></td>
        </tr>
        </c:forEach>
        <br>
  </tbody>
</table>
<br>
<a href="editarProjeto?id=${tarefa.projeto.id_projeto}">Voltar para Projeto</a>

</body>
</html>