<%-- 
    Document   : tarefas_pessoa
    Created on : 29/11/2011, 14:44:24
    Author     : Regis
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="entities.Pessoa"%>
<%@page import="java.util.List"%>
<%@page import="entities.Tarefa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tarefas sob responsabilidade de uma pessoa</title>
    </head>
    <body>
        <h2>Tarefas do(a) ${pessoa.nome}</h2>
        <table style="text-align: left; width: 100%;" border="1" cellpadding="2" cellspacing="2">
          <tbody>
              <c:forEach var="t" items="${tarefasPessoa}">
                <tr>
                    <td><a href="editarTarefa?id=${t.id_tarefa}">${t.nome}</a></td>
                </tr>
                </c:forEach>
                <br>
          </tbody>
        </table>
    </body>
</html>
