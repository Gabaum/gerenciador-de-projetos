<%@page import="entities.Projeto"%>
<%@page import="entities.Tarefa"%>
<%@page import="java.util.List"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- 
    Document   : ver_projetos
    Created on : 23/10/2011, 20:25:18
    Author     : Regis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="custom.css"></link>
        <title>Projetos</title>
    </head>
    <body>
        <div class="geral borders round">
        <h2>Projetos</h2>
            <a href="criarProjeto">Adicionar Projeto</a>
            <table>
                <td></br></td>
                <c:forEach var="projeto" items="${projetos}">
                        <tr><td>${projeto.nome}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><a href="editarProjeto?id=${projeto.id_projeto}">Editar</a></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="excluirProjeto?id=${projeto.id_projeto}">Excluir</a></td></tr>       
                </c:forEach>
            </table>
        </div>
        <a  class="geral" href="index.jsp"> HOME </a>
    </body>
</html>
