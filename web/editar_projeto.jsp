<%@page import="entities.Projeto"%>
<%@page import="entities.Tarefa"%>
<%@page import="java.util.List"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="custom.css"></link>
        <title>${pagename}</title>
    </head>
    <body>
        <div class="geral">
        <h1>${pagename}</h1>
		<form action="salvarProjeto" method="get" class="form round">
			<table>
                            <tr><td>Nome:</td><td><input type="text" name="nome" value="${projeto.nome}"></input></td></tr>
				<tr><td>Objetivo:</td><td><input type="text" name="objetivo" value="${projeto.objetivo}"></input></td></tr>
				<tr><td>Status:</td><td><input type="text" name="status" value="${projeto.status}"></input></td></tr>
			</table>
                        <input type="hidden" name="id" value="${projeto.id_projeto}"></input>
                        <input type="submit" value="Salvar alterações"></input>
               </form>
		
		<h2>Tarefas</h2>
		<a href="adicionarTarefa?id_projeto=${projeto.id_projeto}">Adicionar tarefa</a>
		
		<table class="geral form round">
                    <tr><td>Nome</td><td>Porcentagem Concluida</td><td>Editar</td><td>Excluir</td></tr>
                        <c:forEach var="tarefa" items="${projeto.tarefas}">
			    <tr><td>${tarefa.nome}</td><td>${tarefa.porcentual_concluido}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td><a href="editarTarefa?id=${tarefa.id_tarefa}">Editar</a></td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="excluirTarefa?id=${tarefa.id_tarefa}&id_projeto=${projeto.id_projeto}">Excluir</a></td></tr>
			</c:forEach>
		</table>
        </div>
        <a href="verProjetos" class="geral">Voltar para Projetos</a>
    </body>
</html>