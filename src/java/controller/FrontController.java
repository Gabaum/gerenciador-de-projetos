/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dominio.PessoaDAO;
import dominio.PessoaDAOJPA;
import dominio.ProjetoDAO;
import dominio.ProjetoDAOJPA;
import dominio.TarefaDAO;
import dominio.TarefaDAOJPA;
import entities.Pessoa;
import entities.Projeto;
import entities.Tarefa;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author guilherme
 */
public class FrontController extends HttpServlet {

    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String userPath = request.getServletPath();
        String urlDest = "index.jsp";
        TarefaDAO tdao = new TarefaDAOJPA();
        ProjetoDAO pdao = new ProjetoDAOJPA();
        PessoaDAO pessoadao = new PessoaDAOJPA();
        Calendar c = Calendar.getInstance();

        if(userPath.equals("/verProjetos")){
            List<Projeto> projetos = pdao.listarProjetos();
            request.setAttribute("projetos", projetos);
            urlDest="ver_projetos.jsp";
        }
        
        if(userPath.equals("/criarProjeto")){
           Projeto p = new Projeto();
           p.setNome("Novo Projeto");
           p.setObjetivo("");
           p.setStatus("");
           pdao.inserir(p);
           request.setAttribute("projeto", p);
           request.setAttribute("pagename", "Adicionar Projeto");
           urlDest="editar_projeto.jsp";
        }

        if(userPath.equals("/editarProjeto")){
            int id = Integer.parseInt(request.getParameter("id"));
            Projeto p = pdao.findByID(id);
            request.setAttribute("pagename", "Editar Projeto");
            request.setAttribute("projeto", p);
            urlDest="editar_projeto.jsp";
        }

        if(userPath.equals("/salvarProjeto")){
            String nome = request.getParameter("nome");
            String obj = request.getParameter("objetivo");
            String stat = request.getParameter("status");
            int id = Integer.parseInt(request.getParameter("id"));
            Projeto p = pdao.findByID(id);
            p.setNome(nome);
            p.setObjetivo(obj);
            p.setStatus(stat);
            pdao.salvar(p);
            List<Projeto> projetos = pdao.listarProjetos();
            request.setAttribute("projetos", projetos);
            urlDest="ver_projetos.jsp";
        }
        
        if(userPath.equals("/excluirProjeto")){
            int id = Integer.parseInt(request.getParameter("id"));
            Projeto p = pdao.findByID(id);
            if(p!=null){ 
                pdao.excluir(p);
            }
            List<Projeto> ps = pdao.listarProjetos();
            request.setAttribute("projetos", ps);
            urlDest="ver_projetos.jsp";
        }

        if(userPath.equals("/excluirTarefa")){
            int id = Integer.parseInt(request.getParameter("id"));
            Tarefa t = tdao.getTarefaPorId(id);
            Projeto p = t.getProjeto();
            p.excluirTarefa(t);
            pdao.salvar(p);
            tdao.deletarTarefa(t);
            request.setAttribute("projeto", p);
            request.setAttribute("pagename", "Editar Projeto");
            urlDest="editar_projeto.jsp";
        }
        if(userPath.equals("/adicionarTarefa")){
            int id = Integer.parseInt(request.getParameter("id_projeto"));
            Projeto p = pdao.findByID(id);
            Tarefa t = new Tarefa();
            t.setData_de_inicio(new Date());
            t.setNome("Nova Tarefa");
            t.setPorcentual_concluido(0);
            t.setProjeto(p);
            p.adicionarTarefa(t);
            tdao.inserirTarefa(t);
            pdao.salvar(p);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        if(userPath.equals("/adicionarDependente")){
            int id_tarefa = Integer.parseInt(request.getParameter("id_tarefa"));
            Tarefa t = tdao.getTarefaPorId(id_tarefa);
            int idDep = Integer.parseInt(request.getParameter("idDependencia"));
            Tarefa dep = tdao.getTarefaPorId(idDep);
            t.addDependencia(dep);
            tdao.editarTarefa(t);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        if(userPath.equals("/excluirDependente")){
            int id_tarefa = Integer.parseInt(request.getParameter("id_tarefa"));
            Tarefa t = tdao.getTarefaPorId(id_tarefa);
            int idDep = Integer.parseInt(request.getParameter("idDependencia"));
            Tarefa dep = tdao.getTarefaPorId(idDep);
            t.deleteDependencia(dep);
            tdao.editarTarefa(t);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        if(userPath.equals("/adicionarResponsavel")){
            Tarefa t = tdao.getTarefaPorId(Integer.parseInt(request.getParameter("id_tarefa")));
            int idpessoa = Integer.parseInt(request.getParameter("idPessoa"));
            Pessoa pessoa = pessoadao.getPessoaPorID(idpessoa);
            if(pessoa!=null)
                t.addResponsavel(pessoa);
            tdao.editarTarefa(t);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        
        //<%-- --%>
        if(userPath.equals("/adicionarResponsavelCombobox")){
            Tarefa t = tdao.getTarefaPorId(Integer.parseInt(request.getParameter("id_tarefa")));
            Pessoa  pessoa = pessoadao.getPessoaPorID(Integer.parseInt(request.getParameter("idResponsavel")));
            if(pessoa!=null)
                t.addResponsavel(pessoa);
            tdao.editarTarefa(t);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        //<%-- --%>
        
        if(userPath.equals("/tarefasPessoa")){
            int idPessoa = Integer.parseInt(request.getParameter("idPessoa"));
            Pessoa p = pessoadao.getPessoaPorID(idPessoa);
            request.setAttribute("pessoa", p);
            List<Tarefa> tarefas = tdao.getTarefasPorPessoa(p);
            request.setAttribute("tarefasPessoa", tarefas);
            urlDest="tarefas_pessoa.jsp";
        }
        if(userPath.equals("/excluirResponsavel")){
            Tarefa t = tdao.getTarefaPorId(Integer.parseInt(request.getParameter("id_tarefa")));
            int idPessoa = Integer.parseInt(request.getParameter("idPessoa"));
            Pessoa p = pessoadao.getPessoaPorID(idPessoa);
            t.deleteResponsavel(p);
            tdao.editarTarefa(t);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }
        if(userPath.equals("/editarTarefa")){
            int id  = Integer.parseInt(request.getParameter("id"));
            Tarefa t = tdao.getTarefaPorId(id);
            setEditarTarefa(request,t);
            urlDest="editar_tarefa.jsp";
        }            
        if(userPath.equals("/salvarTarefa")){
            String nome = request.getParameter("nome");
            String data = request.getParameter("dataDeInicio");
            int porcento = parsePorcentagem(request.getParameter("percentualConcluido"));
            Date start = parseDate(data);
            boolean forward = porcento>=0 && start!=null;
            int id = Integer.parseInt(request.getParameter("id"));
            Tarefa t = tdao.getTarefaPorId(id);    
            if(forward){                
                t.setPorcentual_concluido(porcento);
                t.setNome(nome);
                t.setData_de_inicio(start);
                tdao.editarTarefa(t);
                Projeto p = t.getProjeto();
                request.setAttribute("projeto", p);
                request.setAttribute("pagename", "Editar Projeto");
                urlDest = "editar_projeto.jsp";
            }else{
                setEditarTarefa(request, t);
                urlDest="editar_tarefa.jsp";
            }
        }
        try {
            request.getRequestDispatcher(urlDest).forward(request, response);
        } catch (Exception exception) {
        }
   
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }//<editor-fold>

    private void setEditarTarefa(HttpServletRequest request, Tarefa t) {
        Calendar c = Calendar.getInstance();
        c.setTime(t.getData_de_inicio());
        String data = c.get(Calendar.DATE)+"/"+c.get(Calendar.MONTH)+"/"+c.get(Calendar.YEAR);
        request.setAttribute("pessoasNR", t.getNaoResponsaveis());
        request.setAttribute("tarefas", t.getUnrelated());
        request.setAttribute("data", data);
        request.setAttribute("tarefa", t);
    }
       private Date parseDate(String data) {
        Calendar c = Calendar.getInstance();
        try{
            String[] tempo = data.split("/");    
            if(tempo.length!=3)return null;
            Integer date = Integer.parseInt(tempo[0]);
            Integer month = Integer.parseInt(tempo[1]);
            Integer year = Integer.parseInt(tempo[2]);
            if(date<1||month<1||month>12||year<1) return null;
            c.set(Calendar.MONTH, month-1);
            c.set(Calendar.YEAR, year);
            c.set(Calendar.DATE, date);
        }catch(NumberFormatException nfe){
            return null;
        }
        return c.getTime();
    }

    private int parsePorcentagem(String porcento) {
        int ret=-1;
        try{
            ret = Integer.parseInt(porcento);
        }catch(NumberFormatException nfe){
            return -1;
        }
        if(ret<0||ret>100)
            return -1;
        return ret;
    }
}
