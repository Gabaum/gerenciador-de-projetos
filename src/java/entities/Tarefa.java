/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import dominio.PessoaDAO;
import dominio.PessoaDAOJPA;
import dominio.TarefaDAO;
import dominio.TarefaDAOJPA;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Victor
 */
@Entity 
public class Tarefa {
    @Id
    @GeneratedValue
    private int id_tarefa;
    private String nome;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date data_de_inicio;
    private int porcentual_concluido;
    @ManyToOne(optional = false,targetEntity=Projeto.class, fetch= FetchType.EAGER)
    private Projeto projeto;
    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }
    @ManyToMany(targetEntity=Tarefa.class)//, fetch=FetchType.EAGER)
    private List<Tarefa> dependencias;

    @ManyToMany(targetEntity=Pessoa.class)
    private List<Pessoa> responsaveis;

    public Date getData_de_inicio() {
        return data_de_inicio;
    }

    public void setData_de_inicio(Date data_de_inicio) {
        this.data_de_inicio = data_de_inicio;
    }

    public List<Tarefa> getDependencias() {
        if(dependencias==null)dependencias = new ArrayList<Tarefa>();
        return dependencias;
    }

    public void setDependencias(List<Tarefa> dependencias) {
        this.dependencias = dependencias;
    }

    public int getId_tarefa() {
        return id_tarefa;
    }

    public void setId_tarefa(int id_tarefa) {
        this.id_tarefa = id_tarefa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPorcentual_concluido() {
        return porcentual_concluido;
    }

    public void setPorcentual_concluido(int porcentual_concluido) {
        this.porcentual_concluido = porcentual_concluido;
    }

    public List<Pessoa> getResponsaveis() {
        return responsaveis;
    }

    public void addResponsavel(Pessoa p){
        if(responsaveis==null)responsaveis = new ArrayList<Pessoa>();
        for(Pessoa q: responsaveis){
            if(p.getIdPessoa()==q.getIdPessoa())
                return;
        }
        responsaveis.add(p);
    }
    public void deleteResponsavel(Pessoa p){
        Pessoa toremove = null;
        for(Pessoa q: responsaveis){
            if(p.getIdPessoa()==q.getIdPessoa())
                toremove = q;
        }
        if(toremove!=null)responsaveis.remove(toremove);
        
    }

    public List<Tarefa> getUnrelated() {
        TarefaDAO tdao = new TarefaDAOJPA();
        List<Tarefa> tarefas = tdao.getTarefasPorProjeto(this.getProjeto());
        List<Tarefa> toremove = new ArrayList<Tarefa>();
        for(Tarefa t: tarefas){
            if(t.getId_tarefa()==this.id_tarefa)
                toremove.add(t);
            for(Tarefa tt:getDependencias()){
                if(tt.getId_tarefa()==t.getId_tarefa())
                    toremove.add(t);    
            }
        }
        tarefas.removeAll(toremove);
        return tarefas;
    }
    
    //<%-- --%>
    public List<Pessoa> getNaoResponsaveis(){
        PessoaDAO pessoadao = new PessoaDAOJPA();
        List<Pessoa> pessoas = pessoadao.getAll();
        List<Pessoa> toremove = new ArrayList<Pessoa>();
        for(Pessoa p: pessoas){
            if(this.getResponsaveis().contains(p))
                toremove.add(p);
        }
        //pessoas.removeAll(toremove);
        return pessoas;
    }
    //<%-- --%>

    public void addDependencia(Tarefa dep) {
        if(dependencias==null)dependencias = new ArrayList<Tarefa>();
        for(Tarefa t:dependencias)
            if(t.getId_tarefa()==dep.getId_tarefa())
                return;
        dependencias.add(dep);
    }

    public void deleteDependencia(Tarefa dep) {
        if(dependencias==null)return;
        Tarefa torem = null;
        for(Tarefa t:dependencias)
            if(t.getId_tarefa()==dep.getId_tarefa())
                torem = t;
        if(torem!=null)dependencias.remove(torem);
    }
     
}
    
    
    