/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Regis
 */

@Entity
public class Projeto {
    @Id
    @GeneratedValue
    private int id_projeto;
    
    private String nome;
    private String status;
    private String objetivo;
    
    @OneToMany(targetEntity=Tarefa.class)
    private List<Tarefa> tarefas;

    public void setTarefas(List<Tarefa> tarefas) {
        this.tarefas = tarefas;
    }
    
    public int getId_projeto() {
        return id_projeto;
    }

    public void setId_projeto(int id_projeto) {
        this.id_projeto = id_projeto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public List<Tarefa> getTarefas(){
        return tarefas;
    }
    
    public void adicionarTarefa(Tarefa t){
        if(tarefas==null) tarefas = new ArrayList<Tarefa>();
        tarefas.add(t);
    }

    public void excluirTarefa(Tarefa t){
        if(tarefas!=null)tarefas.remove(t);
    }

}