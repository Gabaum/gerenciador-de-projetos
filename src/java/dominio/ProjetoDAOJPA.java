/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import entities.Projeto;
import entities.Tarefa;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import util.JPAUtil;

/**
 *
 * @author Regis
 */
public class ProjetoDAOJPA implements ProjetoDAO{

    @Override
    public List<Projeto> listarProjetos() {
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("Select p from Projeto p");
        List<Projeto> listaProjetos = /*(List<Projeto>)*/ query.getResultList();
        em.close();
        return listaProjetos;
    }

    @Override
    public void salvar(Projeto p) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(p);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void inserir(Projeto p) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void excluir(Projeto p) {
        TarefaDAO tdao = new TarefaDAOJPA();
        List<Tarefa> doProjeto = new ArrayList<Tarefa>(p.getTarefas());
        p.setTarefas(new ArrayList<Tarefa>());
        this.salvar(p);
        for(Tarefa t: doProjeto){
            tdao.deletarTarefa(t);
        }
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        Projeto projeto = em.find(Projeto.class, p.getId_projeto());
        em.remove(projeto);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Projeto findByID(int id) {
        EntityManager em = JPAUtil.getEntityManager();
        Projeto projeto = em.find(Projeto.class, id);
        em.close();
        return projeto;
    }

    @Override
    public Projeto findByName(String name) {
        EntityManager em = JPAUtil.getEntityManager();
        Query q = em.createQuery("Select p from Projeto p where p.nome=:name");
        q.setParameter("name", name);
        Projeto p;
        try{
            p = (Projeto) q.getSingleResult();
        }
        catch(NoResultException e){
            p=null;
        }
        em.close();
        return p;

    }
    
}
