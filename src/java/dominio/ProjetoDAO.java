/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.util.List;
import entities.*;
/**
 *
 * @author Regis
 */
public interface ProjetoDAO {
    public List<Projeto> listarProjetos();
    public void salvar(Projeto p);
    public void inserir(Projeto p);
    public void excluir(Projeto p);
    public Projeto findByID(int id);
    public Projeto findByName(String name);
}
