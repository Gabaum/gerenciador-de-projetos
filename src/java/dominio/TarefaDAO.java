/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import entities.Projeto;
import entities.Tarefa;
import java.util.List;
import entities.Pessoa;

/**
 *
 * @author Victor
 */
public interface TarefaDAO {
    
    public Tarefa getTarefaPorId(int id);
    public void inserirTarefa(Tarefa tarefa);
    public void deletarTarefa(Tarefa tarefa);
    public void editarTarefa(Tarefa tarefa);
    public List<Tarefa> getTarefasPorProjeto (Projeto projeto);
    public List<Tarefa> getTarefasPorPessoa (Pessoa p);
    public List<Tarefa> getTarefasDependentes (Tarefa tarefa);
    public List<Pessoa> getResponsaveis (Tarefa tarefa);
    
    
}
