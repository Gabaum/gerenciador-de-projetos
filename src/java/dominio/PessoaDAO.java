package dominio;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import entities.Pessoa;
import java.util.List;

/**
 *
 * @author ludmilalsp
 */
public interface PessoaDAO {
    public void inserir(Pessoa p);
    public void atualizar(Pessoa p);
    public Pessoa getPessoaPorCPF(String cpf);
    public Pessoa getPessoaPorID(Integer id);
    public List<Pessoa> getAll();
}
