/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import entities.Pessoa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import util.JPAUtil;

/**
 *
 * @author ludmilalsp
 */
public class PessoaDAOJPA implements PessoaDAO {

    @Override
    public void inserir(Pessoa p) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
        em.close();
    }
       
    @Override
    public void atualizar(Pessoa p)
    {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(p);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public Pessoa getPessoaPorCPF(String cpf) {
        Pessoa pessoa = null;
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT p FROM Pessoa p WHERE p.cpf=:cpf");
        query.setParameter("cpf", cpf);
        try {
            pessoa = (Pessoa) query.getSingleResult();
        } catch (Exception e) {}
        return pessoa;
    }
    
    @Override
    public Pessoa getPessoaPorID(Integer id) {
        Pessoa pessoa = null;
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT p FROM Pessoa p WHERE p.idPessoa=:ID");
        query.setParameter("ID", id);
        try {
            pessoa = (Pessoa) query.getSingleResult();
        } catch (Exception e) {}
        return pessoa;
    }
    
    @Override
    public List<Pessoa> getAll(){
        List<Pessoa> pessoas = null;
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT p FROM Pessoa p");
        try {
            pessoas = query.getResultList();
        } catch (Exception e) {}
        return pessoas;        
    }
}
