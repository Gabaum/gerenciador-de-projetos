/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import entities.Pessoa;
import entities.Projeto;
import entities.Tarefa;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import util.JPAUtil;

/**
 *
 * @author Victor
 */
public class TarefaDAOJPA implements TarefaDAO {
    
    
    @Override
    public Tarefa getTarefaPorId(int id) {
        EntityManager em = JPAUtil.getEntityManager();
        Tarefa t = em.find(Tarefa.class, id);
        em.close();
        return t;
    }

    @Override
    public void inserirTarefa(Tarefa tarefa) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.persist(tarefa);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deletarTarefa(Tarefa tarefa) {
        for(Tarefa t: getTarefasPorProjeto(tarefa.getProjeto())){
            t.deleteDependencia(tarefa);
            editarTarefa(t);
        }
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        Tarefa t = em.find(Tarefa.class, tarefa.getId_tarefa());
        em.remove(t);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void editarTarefa(Tarefa tarefa) {
        EntityManager em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        em.merge(tarefa);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<Tarefa> getTarefasPorProjeto(Projeto projeto) {
        List<Tarefa> tarefas = new ArrayList<Tarefa>();
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT t FROM Tarefa t WHERE t.projeto=:p"); 
        query.setParameter("p", projeto);
        tarefas = (List<Tarefa>) query.getResultList();
        em.close();
        return tarefas;
    }

    @Override
    public List<Tarefa> getTarefasPorPessoa(Pessoa p) {
        List<Tarefa> tarefas = new ArrayList<Tarefa>();
        for(Tarefa t: getAllTarefas()){
            for(Pessoa q: t.getResponsaveis()){
                if(q.getIdPessoa()==p.getIdPessoa())
                    tarefas.add(t);
            }
        }
        return tarefas;
    }

    @Override
    public List<Tarefa> getTarefasDependentes(Tarefa tarefa) {
        List<Tarefa> tarefas = new ArrayList<Tarefa>();
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT t FROM Tarefa t, Dependencias d WHERE t.id_tarefa=d.id_tarefa AND d.id_tarefa_dependente=:id"); 
        query.setParameter("id", tarefa.getId_tarefa());
        tarefas = (ArrayList<Tarefa>) query.getResultList();
        em.close();
        return tarefas;
    }

    @Override
    public List<Pessoa> getResponsaveis(Tarefa tarefa) {
        List<Pessoa> pessoas = new ArrayList<Pessoa>();
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT p FROM Pessoa p, Responsaveis r WHERE r.idPessoa=p.idPessoa AND r.id_tarefa=:id"); 
        query.setParameter("id", tarefa.getId_tarefa());
        pessoas = (ArrayList<Pessoa>) query.getResultList();
        em.close();
        return pessoas;
    }

    private List<Tarefa> getAllTarefas() {
        List<Tarefa> t = null;
        EntityManager em = JPAUtil.getEntityManager();
        Query query = em.createQuery("SELECT t FROM Tarefa t");
        try {
            t = query.getResultList();
        } catch (Exception e) {}
        return t; 
        
    }


}
